import React from "react";
import { useState } from "react";
import { useDispatch } from 'react-redux';
import { closeFormAction } from "../actions/index.js";
import { addTaskAction } from "../actions/index.js";

const AddTaskForm = () => {
    const [task, setTask] = useState('');
    const dispatch = useDispatch();
    
    const closeForm = () => {
        dispatch(closeFormAction())
    }
    const addTask = (event) => {
        event.preventDefault()
        const taskItem = {
            task,
            completed: false,
            id: Date.now()
        }
        dispatch(addTaskAction(taskItem))
        dispatch(closeFormAction())
    }

    return(
        <div className="modal">
            <form className="form-task" onSubmit={(event) => addTask(event)}>
                <p className="form-title">New Todo</p>
                <p className="form-pescription">Please write content of todo in input below!</p>
                <input className="form-input" type="text" placeholder="Do something!" value={task}  onChange={(event)=>setTask(event.target.value)}/>
                <div className="form-button-wrapper">
                    <button type="button" className="button form-button form-button--cancel" onClick={closeForm}>Calcel</button>
                    <button type="submit" className="button form-button form-button--add">Add</button>
                </div>
            </form>
        </div>

    )
}
export default AddTaskForm;