import { createStore } from 'redux';
import { combineReducers } from 'redux';
import { formReducer } from './formReducer';
import { taskReducer } from './taskReducer';
import { editFormReducer } from './editFormReducer';

const rootReducer = combineReducers({
    formReducer: formReducer,
    taskReducer: taskReducer,
    editFormReducer: editFormReducer
})

export const store = createStore(rootReducer);
 