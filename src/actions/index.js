import { ADD_TASK } from '../constant/index.js'  
import { REMOVE_TASK } from '../constant/index.js' 
import { COMPLETE_TASK } from '../constant/index.js' 
import { EDIT_TASK } from '../constant/index.js' 
//editFormReducer
import { OPEN_EDIT_FORM } from '../constant/index.js' 
import { CLOSE_EDIT_FORM } from '../constant/index.js' 
//formReducer
import { OPEN_FORM } from '../constant/index.js' 
import { CLOSE_FORM } from '../constant/index.js' 

//taskReducerAction
export const addTaskAction = (payload) => ({type: ADD_TASK, payload})
export const removeTaskAction = (payload) => ({type: REMOVE_TASK, payload})
export const completeTaskAction = (payload) => ({type: COMPLETE_TASK, payload})
export const editeTaskAction = (payload) => ({type: EDIT_TASK, payload})
//editFormReducerAction
export const openEditFormAction = (payload) => ({type: OPEN_EDIT_FORM, payload})
export const closeEditFormAction = (payload) => ({type: CLOSE_EDIT_FORM, payload})
//formReducerAction
export const openFormAction = (payload) => ({type:OPEN_FORM, payload})
export const closeFormAction = (payload) => ({type:CLOSE_FORM, payload})