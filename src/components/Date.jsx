import React from 'react';
import { useState, useEffect } from 'react';

const Date = () => {
    const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const [year, setYear] = useState('')
    const [nowDate, setNowDate] = useState('')
    const [month, setMonth] = useState('')
    const [day, setDay] = useState('')

    const getDate = () => {
        let nowDate = new window.Date()
        setYear(nowDate.getFullYear())
        setNowDate(nowDate.toDateString().slice(8,11))
        setMonth(nowDate.toDateString().slice(3,7))
        setDay(DAYS[nowDate.getDay()])
    }

    useEffect(()=>{
        getDate()
    },[])

    return(
        <div className="date">
            <p className="date-date">{nowDate}</p>
            <div className="date-wrapper">
                <p className="date-month">{month}</p>
                <p className="date-year">{year}</p>
            </div>
            <p className="date-day">{day}</p>
        </div>
    )
}
export default Date;