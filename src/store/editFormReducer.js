import { OPEN_EDIT_FORM } from '../constant/index.js'
import { CLOSE_EDIT_FORM } from '../constant/index.js'

const defaultState = {
    editForm: false,
    editTaskId: ''
}

export const editFormReducer = (state = defaultState, action) => {
        switch (action.type) {
            case OPEN_EDIT_FORM:
                return {
                    ...state,
                    editForm: action.payload.formState,
                    editTaskId: action.payload.TASK_ID
                }
                case CLOSE_EDIT_FORM:
                    return {
                        ...state,
                        editForm: false  
                    }
            default:
                return state
        }
}

