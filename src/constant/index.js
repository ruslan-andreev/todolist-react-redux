//taskReducer
export const ADD_TASK = 'ADD_TASK' 
export const REMOVE_TASK = 'REMOVE_TASK'
export const COMPLETE_TASK = 'COMPLETE_TASK'
export const EDIT_TASK = 'EDIT_TASK'

//editFormReducer
export const OPEN_EDIT_FORM = 'OPEN_EDIT_FORM'
export const CLOSE_EDIT_FORM = 'CLOSE_EDIT_FORM'

//formReducer
export const OPEN_FORM = 'OPEN_FORM'
export const CLOSE_FORM = 'ClOSE_FORM'