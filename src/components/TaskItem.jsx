import React from "react";
import { useDispatch,useSelector } from "react-redux";
import { completeTaskAction, removeTaskAction } from "../actions/index.js";
import { openEditFormAction } from "../actions/index.js";

const TaskItem = ({task, taskId, taskCompleted}) => {
    const TASK = task;
    const TASK_ID = taskId;
    const TASK_COMPLETED = taskCompleted;   
    const dispatch = useDispatch()
    const formEdit = useSelector(state => state.editFormReducer.editForm)

    const removeTask = (TASK_ID) =>{
        dispatch(removeTaskAction(TASK_ID))
    }
    const completeTask = (TASK_ID) => {
        dispatch(completeTaskAction(TASK_ID))
    }
    const openEditForm = (formState, TASK_ID) => {
        dispatch(openEditFormAction({formState, TASK_ID}))
    }

    return(
        <li className="task-list__item">
            <input type="checkbox" checked={TASK_COMPLETED} className="task-checkbox" onChange={()=>completeTask(TASK_ID)}></input>
            <p className={TASK_COMPLETED ? "task--completed" : "task"}>{TASK}</p>
            <button className="button task-button task-button--edit"  onClick={()=>openEditForm(!formEdit, TASK_ID)}>Edit</button>
            <button className="button task-button task-button--delete"  onClick={()=>removeTask(TASK_ID)}>Delete</button>
        </li>
    )
}
export default TaskItem;