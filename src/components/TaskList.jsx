import React from "react";
import TaskItem from "./TaskItem";
import { useSelector } from "react-redux";

const TaskList = () => {
    const taskList = useSelector(state => state.taskReducer.taskList)

    return(
        <ul className="task-list">   
            {taskList.map(task => {
                return <TaskItem 
                    key={task.id}
                    task={task.task}
                    taskId={task.id}
                    taskCompleted={task.completed}
                />
            })    
            }   
        </ul>
    )
}
export default TaskList;
