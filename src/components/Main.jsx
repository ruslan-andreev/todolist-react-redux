import React, { useEffect } from "react";
import Date from './Date';
import TaskList from "./TaskList";
import AddTaskForm from "./AddTaskForm";
import EditTaskForm from "./EditTaskForm";
import EmptyState from "./EmptyState";

import { useDispatch, useSelector } from 'react-redux';
import { openFormAction } from "../actions/index.js";

const Main = () => {
    const dispatch = useDispatch()
    const addForm = useSelector(state => state.formReducer.form)
    const editForm = useSelector(state => state.editFormReducer.editForm)
    const taskList = useSelector(state => state.taskReducer.taskList)

    useEffect(()=>{
        localStorage.setItem('taskList', JSON.stringify(taskList))
    },[taskList])

    const openAddForm = (data) =>{
        dispatch(openFormAction(data))
    }

    return(
        <main className="main">
            <div className="main-container">
                {addForm ? <AddTaskForm /> : ""}
                {editForm ? <EditTaskForm /> : ""}
                <Date />
                {taskList.length > 0 ? <TaskList /> : <EmptyState />}
                <button className="button button-add" onClick={()=>openAddForm(!addForm)}>+</button>
            </div>
        </main>
    )
}
export default Main;