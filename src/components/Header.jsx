import React from "react";

const Header = () => {
    return(
        <header className="header">
            <div className="container">
                <div className="header-wrapper">
                    <h1 className="title">My Todo List</h1>
                </div>
            </div>
        </header>
    )
}
export default Header;