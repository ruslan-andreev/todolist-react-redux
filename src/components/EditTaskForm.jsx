import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { closeEditFormAction } from "../actions/index.js";
import { editeTaskAction } from "../actions/index.js";

const EditTaskForm = () => {
    const [previousTask, setPreviousTask] = useState({})
    const [newTask, setNewTask] = useState('')
    const editTaskId = useSelector(state => state.editFormReducer.editTaskId)
    const taskList = useSelector(state => state.taskReducer.taskList)
    const dispatch = useDispatch();

    const closeForm = () => {
        dispatch(closeEditFormAction())
    }

    const getCurentTask = (editTaskId) =>{
       let task = taskList.filter(task => task.id === editTaskId)
       setPreviousTask(...task)
       setNewTask(...task)
    }

    const editTask = (event, editTaskId, newTask) => {
        event.preventDefault()
        dispatch(editeTaskAction({editTaskId, newTask}))
        dispatch(closeEditFormAction())
    }
    
    useEffect(()=>{
        getCurentTask(editTaskId)  
    },[])
    useEffect(()=>{
        setNewTask(previousTask.task) 
    },[previousTask])

    return(
        <div className="modal">
            <form className="form-task" onSubmit={(event)=>editTask(event, editTaskId, newTask)}>
                <p className="form-title">Edit Task</p>
                <p className="form-pescription">Please edit content of todo in input below!</p>
                <input className="form-input" type="text" value={newTask} onChange={(event)=>setNewTask(event.target.value)}/>
                <div className="form-button-wrapper">
                    <button type="button" className="button form-button form-button--cancel" onClick={closeForm}>Calcel</button>
                    <button type="submit" className="button form-button form-button--edit">Edit</button>
                </div>
            </form>
        </div>
    )
}
export default EditTaskForm;