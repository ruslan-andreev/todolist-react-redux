import { OPEN_FORM } from '../constant/index.js'
import { CLOSE_FORM } from '../constant/index.js'

const defaultState = {
    form: false
}

export const formReducer = (state = defaultState, action) => {
        switch (action.type) {
            case OPEN_FORM:
                return {
                    ...state,
                    form: action.payload
                }
            case CLOSE_FORM:
                return {
                    ...state,
                    form: action.payload    
                }
            default:
                return state
        }
}

