import { ADD_TASK } from '../constant/index.js'
import { REMOVE_TASK } from '../constant/index.js'
import { COMPLETE_TASK } from '../constant/index.js'
import { EDIT_TASK } from '../constant/index.js'

const defultState = {
    taskList: []
}
if(localStorage.getItem('taskList') !== null){
    defultState.taskList = JSON.parse(localStorage.getItem('taskList'))
}

export const taskReducer = (state = defultState, action) => {
    switch(action.type){

        case ADD_TASK:
            return {
                ...state,
                taskList: [...state.taskList, action.payload]
            }
        case REMOVE_TASK:
            return {
                ...state,
                taskList: state.taskList.filter(task => task.id !== action.payload)
            }
        case COMPLETE_TASK:
            return {
               ...state,
               taskList: state.taskList.map(task => {
                   if(task.id == action.payload){
                       return {
                           ...task, completed: !task.completed
                       }
                   }
                   return task
               })   
            }
        case EDIT_TASK:
            return {
                ...state,
                taskList: state.taskList.map(task => {
                    if(task.id == action.payload.editTaskId){
                        return {
                            ...task, task: action.payload.newTask
                        }
                    }
                    return task
                })
            }  
        default:
            return state;
    }
}

