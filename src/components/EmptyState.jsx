import React from "react";

const EmptyState = () => {
    return(
        <div className="empty-state">
            <h2 className="empty-state__content">Task list is empty</h2>
        </div>
    )
}
export default EmptyState;