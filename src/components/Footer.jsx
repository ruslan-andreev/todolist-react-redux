import React from "react";

const Footer = () => {
    return(
        <div className="footer">
            <div className="container">
                <p className="copyright">Ruslan Andreev Minsk 2022</p>
            </div>
        </div>
    )
}
export default Footer;